<?php

    define("LABTITLE", "Midterm - Practical Exam");
    define("DESCRIPTION", "Square and Cube of a Number");
    define("CURRENT", 'pe/input.php');
    define("IS_ERROR_PAGE", true);
?>

<?php require('../.includes/header.php'); ?>

          <div id="title" class="hero-unit">
            <h1>Warning: No output to generate.</h1>
            <p>You either typed an invalid number, or failed to check even a single checkbox.</p>
            <hr />
 <a href="./input.php" class="btn btn-orange pull-right"><i class="icon-white icon-arrow-left"></i> Back</a>

          </div>

<?php require('../.includes/footer.php'); ?>