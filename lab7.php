<?php

define("LABTITLE", "Laboratory Activity No. 7");
define("DESCRIPTION", "'Regular Expression'");
define("CURRENT", 'lab7.php');
define("IS_ERROR_PAGE", false);
?>

<?php
  // SUPPLY THE MISSING PATTERN
  // $patterns IS A TWO DIMENSIONAL ARRAY CONTAINING [description, regex]
  $patterns[] = array('String containing "PHP"', 'PHP');
  $patterns[] = array('Starting with "PHP"', '^PHP');
  $patterns[] = array('Ends with "PHP"', 'PHP$');
  $patterns[] = array('String equal to "PHP"', '^PHP$');
  $patterns[] = array('Word starting with letter "C"', '^C[a-zA-Z]+$');
  $patterns[] = array('Non-empty lowercase string', '^[a-z]+$');
  $patterns[] = array('Non-empty uppercase string', '^[A-Z]+$');
  $patterns[] = array('Minimum 10 letters Maximum 20 letters', '^[a-zA-Z]{10,20}$');
  $patterns[] = array('Minimum 10 digits Maximum 20 digits', '^\d{10,20}$');
  $patterns[] = array('Minimum 10 characters Maximum 20 characters', '^.{10,20}$');
  $patterns[] = array('Valid PHP variable name', '^\$[a-zA-Z_]\w*$');
  $patterns[] = array('Valid PHP constant name', '^[a-zA-Z_]\w*$');
  $patterns[] = array('Valid decimal (base-10) integer', '^(-)?\d+$');
  $patterns[] = array('Valid float number', '^\d+\.\d+$');
  $patterns[] = array('5-letter string', '^[a-zA-Z]{5}$');
  $patterns[] = array('5-digit string', '^[0-9]{5}$');
  $patterns[] = array('5 ascii characters', '^.{5}$');
  $patterns[] = array('5 non-alphanumeric characters', '^[^a-zA-Z0-9]{5}$');
  $patterns[] = array('Passing Grade (1.00,1.25,1.50 to 3.00)', '(^(1|2)\.(00|25|50|75)$)|(3\.00)');
  $patterns[] = array('Sub-domain Name (Ex. .edu or .feu-eastasia.edu or .ite.feu-eastasia.edu)', '^(\.[a-zA-Z0-9-]+)+$');

?>

<?php require('.includes/header.php'); ?>
      <div class="tab-content">
        <div class="tab-pane fade" id="spec">
          <h2>Regular Expression</h2>
          <p>The objective of this lab activity is to get acquainted with PHP's regular expression.</p>
          <p>Download the full pdf instructions here : <br />
            <a href="https://www.dropbox.com/s/pyzookgfspc2vvu/itwp103_itwa133_lab_07_regular_expressions.doc" title="<?php echo LABTITLE . " - " . DESCRIPTION; ?>">
            <?php echo LABTITLE . " - " . DESCRIPTION; ?>
            </a>
            .</p>
        </div>
        <div class="tab-pane fade in active" id="app">
          <h3>Regular Expression in PHP<small>&rarr; There's always a pattern.</small></h3>
            <form action="lab7.php" method="post">
          <table id="lab7" class="table table-bordered">
<tr>
  <th>#</th>  <th>Description</th> <th>String</th>  <th>RegEx Pattern</th>  <th>Result</th>
</tr>
<?php
$c = 1;
foreach($patterns as $pattern) : 
  $pattern_desc = $pattern[0];
  $pattern_exp  = $pattern[1];
  $value        = isset($_POST["field{$c}"]) ? $_POST["field{$c}"] : '';

  if ($pattern_exp == '') {
        $result = 'Missing pattern';
        $pattern = '&nbsp;';
  } else {
        $pattern     = '/' . $pattern_exp . '/';
      if (preg_match($pattern, $value))
        $result = 'Valid';
      else
        $result = 'Invalid';
    }
  ?>
  <tr class=<?php if($result == 'Valid') echo 'valid'; else echo 'invalid'; ?>>
    <td><?php echo $c; ?></td>
    <td><?php echo $pattern_desc; ?></td>
    <td><input type="text" name="<?php echo "field{$c}"; ?>" value="<?php echo htmlspecialchars($value); ?>"</td>
    <td><?php echo $pattern ?></td>
    <td><?php echo $result; ?></td>
  </tr>
<?php $c++; endforeach; ?>
          </table>
<br/>

            <button type="submit" class="btn btn-orange" name="validate" ><i class="icon-ok icon-white"></i> Validate</button>
            <button type="reset" class="btn btn-danger" ><i class="icon-refresh icon-white"></i> Reset</button>
          </form>
        </div>


<?php require('.includes/footer.php'); ?>