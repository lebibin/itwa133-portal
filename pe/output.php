<?php
if ( empty($_POST['number']) || empty($_POST['pow']))
  header('Location: ./error.php');

    define("LABTITLE", "Midterm - Practical Exam");
    define("DESCRIPTION", "Square and Cube of a Number");
    define("CURRENT", 'pe/input.php');
    define("IS_ERROR_PAGE", false);
    
    ?>
<?php
  $square = false;  $cube = false; $fourth = false; $fifth = false; $tenth = false;
  if( ! empty($_POST['number']) &&  $_POST['number'] > 0 && isset($_POST['pow']) && $_POST['number'] <= 25) {
    $num = strip_tags($_POST['number']);
    if(in_array('square', $_POST['pow']))
      $square = true;
    if(in_array('cube', $_POST['pow']))
      $cube = true;
    if(in_array('fourth', $_POST['pow']))
      $fourth = true;
    if(in_array('fifth', $_POST['pow']))
      $fifth = true;
    if(in_array('tenth', $_POST['pow']))
      $tenth = true;
  } else {
    header('Location: ./error.php');
  }
?>
<?php require('../.includes/header.php'); ?>

<div class="tab-content">
    <div class="tab-pane fade" id="spec">
          <h2>Square and Cube of a Number</h2>
          <p>Write a PHP script to produce a neatly printed table containing the squares and cubes of numbers from
1 to 25 only.</p>
          <p>Download the full pdf instructions here : <br />
            <a href="https://www.dropbox.com/s/lut0mgwzh8fp8es/itwp103_itwa133_midterm_practical_exam.pdf" title="<?php echo LABTITLE . " - " . DESCRIPTION; ?>">
            <?php echo LABTITLE . " - " . DESCRIPTION; ?>
            </a>
            .</p>
    </div>
    <div class="tab-pane fade in active" id="app">
    <div id="pe">
        <h3>Exponents.<small>&rarr; Same old. Same old.</small></h3>
        <hr/>
        <table class="table table-striped">
          <tr>
            <th>Number ( n )</th>
            <?php
            if($square) echo '<th>( n<sup>2</sup> )</th>';
            if($cube) echo '<th>( n<sup>3</sup> )</th>';
            if($fourth) echo '<th>( n<sup>4</sup> )</th>';
            if($fifth) echo '<th>( n<sup>5</sup> )</th>';
            if($tenth) echo '<th>( n<sup>10</sup> )</th>';
            ?>
          </tr>
        <?php
          for($i = 1; $i <= $num; $i++) :
        ?>
          <tr>
            <td>
              <?php echo $i; ?>
            </td>
        <?php
          if($square) :
        ?>
            <td>
              <?php echo pow($i, 2); ?>
            </td>

        <?php endif; ?>
        <?php if($cube) : ?>
            <td>
              <?php echo pow($i, 3); ?>
            </td>
        <?php endif; ?>
        <?php if($fourth) : ?>
            <td>
              <?php echo pow($i, 4); ?>
            </td>
        <?php endif; ?>
        <?php if($fifth) : ?>
            <td>
              <?php echo pow($i, 5); ?>
            </td>
        <?php endif; ?>
        <?php if($tenth) : ?>
            <td>
              <?php echo pow($i, 10); ?>
            </td>
        <?php endif; ?>
      </tr>
        <?php endfor; ?>
         </table>
        <hr />
      <a href="./input.php" class="btn btn-orange pull-right"><i class="icon-white icon-arrow-left"></i> Back</a>
    </div>
    </div>



<?php require('../.includes/footer.php'); ?>