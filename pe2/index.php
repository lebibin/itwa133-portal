<?php session_start(); 

    define("LABTITLE", "Mini - Flight Reservation");
    define("DESCRIPTION", "WEBPROG Travel Agency");
	define("CURRENT", 'pe2/index.php');
    define("IS_ERROR_PAGE", false);

    if( isset($_SESSION['customer']) ) {
        session_destroy();
    }

    #   VALIDATION START
    $errors = array();
    $errorsCtr = 0;


    #   NAME
    $name = isset($_POST['name']) ? strip_tags($_POST['name']) : '';
    $errors['name'] = array();
    if(empty($name)){
        array_push($errors['name'], 'Name is required.');
        $errorsCtr++;
    }
    if(! (strlen($name) < 32 && strlen($name) > 5)){
        array_push($errors['name'], 'Name\'s length is invalid.');
        $errorsCtr++;
    }
    if( preg_match('/\d/', $name)){
        array_push($errors['name'], 'Name must not contain any number.');
        $errorsCtr++;
    }



    #   ADDRESS
    $address = isset($_POST['address']) ? strip_tags($_POST['address']) : '';
    $errors['address'] = array();
    if(empty($address)){
        array_push($errors['address'], 'Address is required.');
        $errorsCtr++;
    }
    if(! (strlen($address) < 64 && strlen($address) > 5)){
        array_push($errors['address'], 'Address\' length is invalid.');
        $errorsCtr++;
    }

    #   ORIGIN
    $addToAbb = array(
        'MNL'               =>  'Manila',
        'MBT'               =>  'Masbate',
        'PPS'               =>  'Puerto Princesa',
        'SFE'               =>  'San Fernando',
        'SJI'               =>  'San Jose'
    );
    $origin = isset($_POST['origin']) ? strip_tags($_POST['origin']) : '';

    $fid = isset($_POST['fid']) ? strip_tags($_POST['fid']) : '';
    $errors['origin'] = array();
    if(! empty($fid)) {
        $destIndex = substr($fid, 4);
        $destination = $addToAbb[$destIndex];
        if(isset($addToAbb[$destIndex]) && $addToAbb[$destIndex] == $origin) {
            array_push($errors['origin'], 'Origin is same as destination.');
            $errorsCtr++;
        }
    }


    #   CONTACT NUMBER
    $llnum = isset($_POST['llnum']) ? strip_tags($_POST['llnum']) : '';
    $errors['llnum'] = array();
    if(empty($llnum)){
        array_push($errors['llnum'], 'Landline number is required.');
        $errorsCtr++;
    }
    if(! preg_match('/^\d{3}-?\d{4}$/', $llnum)){
        array_push($errors['llnum'], 'Invalid landline number format.');
        $errorsCtr++;
    }

    $cpnum = isset($_POST['cpnum']) ? strip_tags($_POST['cpnum']) : '';
    $errors['cpnum'] = array();
    if(empty($cpnum)){
        array_push($errors['cpnum'], 'Cellphone number is required.');
        $errorsCtr++;
    }
    if(! preg_match('/^((\+639)|(09))\d{9}$/', $cpnum)){
        array_push($errors['cpnum'], 'Invalid cellphone number format.');
        $errorsCtr++;
    }

    #   BAGGAGE
    $trunk = false; $suitcase = false;  $wheeled = false;   $garment = false;   $duffel = false;
    if(isset($_POST['baggage'])){
        $baggage = array();
        $errors['baggage'] = array();
        $errors['weight'] = array();
        $totalWeight = 0;

        if(in_array('Trunk', $_POST['baggage'])){
          $baggage[] = 'Trunk';
            $trunk = true;
            if(isset($_POST['trunkWeight'])) {
                if($_POST['trunkWeight'] < 0) {
                    array_push($errors['weight'], 'Trunk\'s Weight should not be less than 0.');
                    $errorsCtr++;
                } else {
                    $trunkWeight = $_POST['trunkWeight'];
                    $totalWeight += $trunkWeight;
                }
            }
        }

        if(in_array('Suitcase', $_POST['baggage'])){
          $baggage[] = 'Suitcase';
          $suitcase = true;
            if(isset($_POST['suitcaseWeight'])) {
                if($_POST['suitcaseWeight'] < 0) {
                    array_push($errors['weight'], 'Suitcase\'s Weight should not be less than 0.');
                    $errorsCtr++;
                } else {
                    $suitcaseWeight = $_POST['suitcaseWeight'];
                    $totalWeight += $suitcaseWeight;
                }
            }
        }

        if(in_array('Wheeled Upright', $_POST['baggage'])){
          $baggage[] = 'Wheeled Upright';
          $wheeled = true;
            if(isset($_POST['wheeledWeight'])) {
                if($_POST['wheeledWeight'] < 0) {
                    array_push($errors['weight'], 'Wheeled Upright\'s Weight should not be less than 0.');
                    $errorsCtr++;
                } else {
                    $wheeledWeight = $_POST['wheeledWeight'];
                    $totalWeight += $wheeledWeight;
                }
            }
        }

        if(in_array('Garment Bag', $_POST['baggage'])){
          $baggage[] = 'Garment Bag';
          $garment = true;
            if(isset($_POST['garmentWeight'])) {
                if($_POST['garmentWeight'] < 0) {
                    array_push($errors['weight'], 'Garment Bag\'s Weight should not be less than 0.');
                    $errorsCtr++;
                } else {
                    $garmentWeight = $_POST['garmentWeight'];
                    $totalWeight += $garmentWeight;
                }
            }
        }

        if(in_array('Duffel Bag', $_POST['baggage'])){
          $baggage[] = 'Duffel Bag' ;
          $duffel = true;
            if(isset($_POST['duffelWeight'])) {
                if($_POST['duffelWeight'] < 0) {
                    array_push($errors['weight'], 'Duffel Bag\'s Weight should not be less than 0.');
                    $errorsCtr++;
                } else {
                    $duffelWeight = $_POST['duffelWeight'];
                    $totalWeight += $duffelWeight;

                }
            }
        }

        if($totalWeight > 20) {
            array_push($errors['weight'], 'Total weight should not exceed 20kg.');
            $errorsCtr++;
        }
    } else {
        $errors['baggage'] = array();
        array_push($errors['baggage'], 'No baggage declared.');
        $errorsCtr++;
    }

    #   COMPANION 1
    $companion1 = isset($_POST['companion1']) ? strip_tags($_POST['companion1']) : '';
    $errors['companion1'] = array();

    #   COMPANION 2
    $companion2 = isset($_POST['companion2']) ? strip_tags($_POST['companion2']) : '';
    $errors['companion2'] = array();

    #   FLIGHT
    $flight = isset($_POST['flight']) ? strip_tags($_POST['flight']) : '';
    $errors['flight'] = array();


    if(!empty($_POST) && !$errorsCtr ){
        $_SESSION['customer'] = array(
            'Origin'            =>  $origin,
            'Destination'       =>  $destination,
            'Name'              =>  $name,
            'Address'           =>  $address, 
            'Landline Number'   =>  $llnum,
            'Cellphone Number'  =>  $cpnum, 
            'Flight Direction'  =>  $flight,
            'Companion 1'       =>  $companion1,
            'Companion 2'       =>  $companion2,  
            'Baggages'          =>  $baggage,
            'Total Weight'      =>  $totalWeight,
        );
        header('Location: http://' . $_SERVER['SERVER_NAME'] . '/pe2/result.php');
    }

?>

<?php require('../.includes/header.php'); ?>

<div class="tab-content">
    <div class="tab-pane fade" id="spec">
          <h2>WEBPROG Travel Agency</h2>
          <p>The objective of this lab activity is to create a Flight Reservation 'minisystem' that'll focus on form validation and calculating values from form inputs..</p>
          <p>Download the full pdf instructions here : <br />
            <a href="https://www.dropbox.com/s/8brwpvtasufxxzp/itwp103_practical_exam_22.pdf" title="<?php echo LABTITLE . " - " . DESCRIPTION; ?>">
            <?php echo LABTITLE . " - " . DESCRIPTION; ?>
            </a>
            .</p>
    </div>
    <div class="tab-pane fade in active" id="app">
<h3>Available Flights</h3>
<hr />
<?php if( !empty($errors) && !empty($_POST) && $errorsCtr) : ?>
    <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4>There seems to be a problem, mate!</h4>
        <ul>
<?php
    foreach($errors as $key => $value) :
?>
        <?php foreach($errors[$key] as $field => $message) : ?>
        <li><?php echo $message; ?></li>
        <?php endforeach; ?>
<?php
    endforeach; ?>
        </ul>
    </div>
    <?php
endif; ?>
<table class="table table-bordered pe2">
	<thead>
	<tr>
		<th>MANILA TO MASBATE (MNL - MBT)</th>
		<th>MASBATE TO MANILA (MBT - MNL)</th>
		<th></th>
		<th></th>
	</tr>
</thead>
	<tr>
		<td id="MNL-MBT" class="sched">Z2 260 - DAILY - 0510 to 0630 <i class="icon-chevron-right pull-right"></i> </td>
		<td id="MBT-MNL" class="sched">Z2 261 - DAILY - 0705 to 0825 <i class="icon-chevron-right pull-right"></i> </td>
		<td>MA60</td>
		<td>10 Kgs</td>
	</tr>
</table>

<table class="table table-bordered pe2">
	<thead>
	<tr>
		<th>MANILA TO PUERTO PRINCESA (MNL - PPS)</th>
		<th>PUERTO PRINCESA TO MANILA (PPS - MNL)</th>
		<th></th>
		<th></th>
	</tr>
</thead>
	<tr>
		<td id="MNL-PPS" class="sched">Z2 422 - DAILY - 0740 to 0855 <i class="icon-chevron-right pull-right"></i> </td>
		<td id="PPS-MNL" class="sched">Z2 423 - DAILY - 0930 to 1045 <i class="icon-chevron-right pull-right"></i> </td>
		<td>A320</td>
		<td>15 Kgs</td>
	</tr>
	<tr>
		<td class="sched">Z2 420 - DAILY - 1530 to 1645 <i class="icon-chevron-right pull-right"></i> </td>
		<td class="sched">Z2 421 - DAILY - 1725 to 1840 <i class="icon-chevron-right pull-right"></i> </td>
		<td>A320</td>
		<td>15 Kgs</td>
	</tr>
</table>

<table class="table table-bordered pe2">
	<thead>
	<tr>
		<th>MANILA TO SAN FERNANDO (MNL - SFE)</th>
		<th>SAN FERNANDO TO MANILA (SFE - MNL)</th>
		<th></th>
		<th></th>
	</tr>
</thead>
	<tr>
		<td id="MNL-SFE" class="sched">Z2 170 - MO/WE/FR - 1310 to 1425 <i class="icon-chevron-right pull-right"></i> </td>
		<td id="SFE-MNL" class="sched">Z2 171 - MO/WE/FR - 1505 to 1620 <i class="icon-chevron-right pull-right"></i> </td>
		<td>MA60</td>
		<td>10 Kgs</td>
	</tr>
</table>

<table class="table table-bordered pe2">
	<thead>
	<tr>
		<th>MANILA TO SAN JOSE (MNL - SJI)</th>
		<th>SAN JOSE TO MANILA (SJI - MNL)</th>
		<th></th>
		<th></th>
	</tr>
</thead>
	<tr>
		<td id="MNL-SJI" class="sched">Z2 400 - DAILY - 0500 to 0555 <i class="icon-chevron-right pull-right"></i> </td>
		<td id="SJI-MNL" class="sched">Z2 401 - DAILY - 0635 to 0730 <i class="icon-chevron-right pull-right"></i> </td>
		<td>MA60</td>
		<td>10 Kgs</td>
	</tr>
</table>

<div id="customerForm" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="formLabel">&#8220; Let's get to know you better!&#8221;</h3>
  </div>
  <div class="modal-body">
<form id="custForm" action="index.php" method="post" class="form-horizontal">
        <div class="control-group <?php if(!empty($origin) && empty($errors['origin'])) echo 'success'; if( !empty($origin) && !empty($errors['origin']) ) echo 'error'; ?>">
            <label class="control-label" for="number">You're currently near?</label>
                <div class="controls">
                    <select name='origin'>
                        <option <?php if(isset($origin) && $origin == 'Manila') echo 'selected'; ?> value='Manila'>Manila</option>
                        <option <?php if(isset($origin) && $origin == 'Masbate') echo 'selected'; ?> value='Masbate'>Masbate</option>
                        <option <?php if(isset($origin) && $origin == 'Puerto Princesa') echo 'selected'; ?> value='Puerto Princesa'>Puerto Princesa</option>
                        <option <?php if(isset($origin) && $origin == 'San Fernando') echo 'selected'; ?> value='San Fernando'>San Fernando</option>
                        <option <?php if(isset($origin) && $origin == 'San Jose') echo 'selected'; ?> value='San Jose'>San Jose</option>
                    </select>
                </div>
        </div>

        <div class="control-group <?php if(!empty($name) && empty($errors['name'])) echo 'success'; if( !empty($name) && !empty($errors['name']) ) echo 'error'; ?>">
            <label class="control-label" for="number">What's your name?</label>
                <div class="controls">
            <input type="text" required class="required" name="name" <?php if(!empty($name) ) echo htmlspecialchars("value='{$name}'"); else echo 'placeholder="Juan Dela Cruz"'; ?>>
                </div>
        </div>

        <div class="control-group <?php if(!empty($address) && empty($errors['address'])) echo 'success'; if( !empty($address) && !empty($errors['address']) ) echo 'error'; ?>">
            <label class="control-label" for="number">Where you from?</label>
                <div class="controls">
            <input type="text" required class="required" name="address" <?php if(!empty($address) ) echo htmlspecialchars("value='{$address}'"); else echo 'placeholder="123 Malungay St. Manila"'; ?>>
                </div>
        </div>

        <div class="control-group <?php if(!empty($llnum) && empty($errors['llnum'])) echo 'success'; if( !empty($llnum) && !empty($errors['llnum']) ) echo 'error'; ?>">
            <label class="control-label" for="llnum">How to contact you?</label>
                <div class="controls">
            <input type="text" required class="required" name="llnum" <?php if(!empty($llnum) ) echo htmlspecialchars("value='{$llnum}'"); else echo 'placeholder="656-7913"'; ?>>
                </div>
        </div>

        <div class="control-group <?php if(!empty($cpnum) && empty($errors['cpnum'])) echo 'success'; if( !empty($cpnum) && !empty($errors['cpnum']) ) echo 'error'; ?>">
            <label class="control-label" for="cpnum"></label>
                <div class="controls">
            <input type="text" required class="required" name="cpnum" <?php if(!empty($cpnum) ) echo htmlspecialchars("value='{$cpnum}'"); else echo 'placeholder="+639351234567"'; ?>>
                </div>
        </div>


        <div class="control-group <?php if(!empty($companion1) && empty($errors['companion1'])) echo 'success'; if( !empty($companion1) && !empty($errors['companion1']) ) echo 'error'; ?>">
            <label class="control-label" for="number">Traveling with someone?</label>
                <div class="controls">
            <label class="radio">
            <input type="radio" <?php if(!isset($companion1) || $companion1 == 'No' || empty($companion1)) echo 'checked'; ?> name="companion1" value="No">No, going solo</label>
            <label class="radio">
            <input type="radio" <?php if(isset($companion1) && $companion1 == 'Child') echo 'checked'; ?> name="companion1" value="Child">Baby, Child, Infant</label>
            <label class="radio">
            <input type="radio" <?php if(isset($companion1) && $companion1 == 'Adult') echo 'checked'; ?> name="companion1" value="Adult">Friend, Colleague</label>
            <label class="radio">
            <input type="radio" <?php if(isset($companion1) && $companion1 == 'Senior Citizen') echo 'checked'; ?> name="companion1" value="Senior Citizen">Grandma, Grandpa</label>
                </div>
        </div>

        <div class="control-group <?php if(!empty($companion2) && empty($errors['companion2'])) echo 'success'; if( !empty($companion2) && !empty($errors['companion2']) ) echo 'error'; ?>">
            <label class="control-label" for="number">...and another one?</label>
                <div class="controls">
            <label class="radio">
            <input type="radio" <?php if(!isset($companion2) || $companion2 == 'No' || empty($companion2)) echo 'checked'; ?> name="companion2" value="No">No, going solo</label>
            <label class="radio">
            <input type="radio" <?php if(isset($companion2) && $companion2 == 'Child') echo 'checked'; ?> name="companion2" value="Child">Baby, Child, Infant</label>
            <label class="radio">
            <input type="radio" <?php if(isset($companion2) && $companion2 == 'Adult') echo 'checked'; ?> name="companion2" value="Adult">Friend, Colleague</label>
            <label class="radio">
            <input type="radio" <?php if(isset($companion2) && $companion2 == 'Senior Citizen') echo 'checked'; ?> name="companion2" value="Senior Citizen">Grandma, Grandpa</label>
                </div>
        </div>

        <div class="control-group <?php if(!empty($baggage) && empty($errors['baggage'])) echo 'success'; if( !empty($baggage) && !empty($errors['baggage']) ) echo 'error'; ?>">
            <label class="control-label" for="number">What are you bringing?</label>
                <div class="controls">
            <div>
                <label class="checkbox"><input type="checkbox" name="baggage[]" value="Trunk" <?php if(isset($trunkWeight) ) echo 'checked'; ?>>Trunk</label>
                <div class='input-append'>
                    <input type="text" class='span1' name='trunkWeight' <?php if(isset($trunkWeight) ) echo htmlspecialchars("value='{$trunkWeight}'"); ?>>
                    <span class='add-on'>kg</span>
                </div>
            </div>

            <div>
            <label class="checkbox"><input type="checkbox" name="baggage[]" value="Suitcase" <?php if(isset($suitcaseWeight) ) echo 'checked'; ?>>Suitcase</label>
                <div class='input-append'>
                    <input type="text" class='span1' name='suitcaseWeight' <?php if(isset($suitcaseWeight) ) echo htmlspecialchars("value='{$suitcaseWeight}'"); ?>>
                    <span class='add-on'>kg</span>
                </div>
            </div>

            <div>
                <label class="checkbox"><input type="checkbox" name="baggage[]" value="Wheeled Upright" <?php if(isset($wheeledWeight) ) echo 'checked'; ?>>Wheeled Upright</label>
                <div class='input-append'>
                    <input type="text" class='span1' name='wheeledWeight' <?php if(isset($wheeledWeight) ) echo htmlspecialchars("value='{$wheeledWeight}'"); ?>>
                    <span class='add-on'>kg</span>
                </div>
            </div>

            <div>
                <label class="checkbox"><input type="checkbox" name="baggage[]" value="Garment Bag" <?php if(isset($garmentWeight) ) echo 'checked'; ?>>Garment Bag</label>
                <div class='input-append'>
                    <input type="text" class='span1' name='garmentWeight' <?php if(isset($garmentWeight) ) echo htmlspecialchars("value='{$garmentWeight}'"); ?>>
                    <span class='add-on'>kg</span>
                </div>
            </div>


            <div>
                <label class="checkbox"><input type="checkbox" name="baggage[]" value="Duffel Bag" <?php if(isset($duffelWeight) ) echo 'checked'; ?>>Duffel Bag</label>
                <div class='input-append'>
                    <input type="text" class='span1' name='duffelWeight' <?php if(isset($duffelWeight) ) echo htmlspecialchars("value='{$duffelWeight}'"); ?>>
                    <span class='add-on'>kg</span>
                </div>
            </div>

                </div>
        </div>

        <div class="control-group <?php if(!empty($flight) && empty($errors['flight'])) echo 'success'; if( !empty($flight) && !empty($errors['flight']) ) echo 'error'; ?>">
            <label class="control-label" for="number">Type of Reservation</label>
                <div class="controls">
            <label class="radio">
            <input <?php if(!isset($flight) || $flight == 'No' || empty($flight)) echo 'checked'; ?> type="radio" name="flight" value="One-Way">One-Way</label>
            <label class="radio">
            <input <?php if(isset($flight) && $flight =='Two-Way' ) echo 'checked'; ?> type="radio" name="flight" value="Two-Way">Two-Way</label>
                </div>
        </div>

        <input type="hidden" name='fid' value=''> 

  </div>
  <div class="modal-footer">
    <button type="submit" class="btn btn-orange"><i class="icon-white icon-ok"></i> Submit</button>
           </form>
  </div>
</div>

    </div>
<?php require('../.includes/footer.php'); ?>