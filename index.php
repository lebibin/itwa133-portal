<?php

define("LABTITLE", "Simple Home Page");
define("DESCRIPTION", "Create a Simple Home Page for your Portal");
define("CURRENT", '/');
define("IS_ERROR_PAGE", false);
?>

<?php require('.includes/header.php'); ?>

<?php $sites = array(
    //'Coffeescript'        =>  'http://coffeescript.org/',
    //'Ruby'                =>  'http://www.ruby-lang.org/en/',
    //'Backbone.js'         =>  'http://backbonejs.org/',
    //'Laravel'             =>  'http://laravel.com/',
    //'Sublime Text 2'      =>  'http://sublimetext.com/2',
    //'Ubuntu'              =>  'http://www.ubuntu.com/',
    // 'github'              =>  'https://github.com/kevdashdev',
    // 'bitbucket'           =>  'https://bitbucket.org/kevdashdev',
    'dropbox'             =>  'http://db.tt/Cfrkry2',
    'asmallorange'        =>  'http://asmallorange.com/',
  );
$site_heading = array_rand($sites);
$site_link = $sites[$site_heading];

$r_link = array_rand($links);

?>
          <div id="title" class="hero-unit">
            <h1>&#8220; Blog about <a href="<?php echo $site_link; ?>" title="<?php echo $site_heading; ?>"><?php echo $site_heading; ?></a> &#8221;</h1>
            <p><small>No, silly! This is <strong>not</strong> really a blog about <?php echo $site_heading; ?>.</small></p>
            <hr />
            <p>What it is? A compilation of the activities we've done at school in our PHP Web Programming class.</p>
            <p>Still interested? <del>Really?!</del> Go ahead and check out an <a href="<?php echo $r_link; ?>" title="<?php echo "Laboratory Activity - $links[$r_link]"; ?>">example</a>.</p>
          </div>

<?php require('.includes/footer.php'); ?>