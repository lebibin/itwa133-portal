<?php

define("LABTITLE", "Laboratory Activity No. 6");
define("DESCRIPTION", "String Functions in PHP");
define("CURRENT", 'lab6.php');
define("IS_ERROR_PAGE", false);
?>

<?php
  $string = (isset($_GET['string'])) ? $_GET['string'] : ' an online guide to html form <input> tag ';
  if(empty($string) || preg_match("/^\s+$/", $string)) {
    $error = "You did not enter any string. Was that intentional by any means?";
  }

  $booleanArray = array(
    false     =>  '(boolean) false',
    true      =>  '(boolean) true'
  );

  $output = array(
    'Original value of $string'                    =>  $string,
    'Number of characters'                         =>  strlen($string),
    'Number of words'                              =>  count(preg_split("/[\s]+/", trim($string))),
    'First character to uppercase'                 =>  ucfirst($string),
    'First character of each word to uppercase'    =>  ucwords($string),
    'To lowercase'                                 =>  strtolower($string),
    'To uppercase'                                 =>  strtoupper($string),
    'Without leading spaces'                       =>  ltrim($string),
    'Without trailing spaces'                      =>  rtrim($string),
    'Without leading spaces and trailing spaces'   =>  trim($string),
    'MD5 value of the $string'                     =>  MD5($string),
    'Base64 value of the $string'                  =>  base64_encode($string),
    'First 16 characters'                          =>  substr($string, 0, 16),
    'Last 4 characters'                            =>  substr($string, -4),
    '4 characters starting from the 20th position' =>  substr($string, 19, 5),
    'Position of the word "guide"'                 =>  strpos(strtolower($string), 'guide'),
    'Position of the word "UE"'                    =>  strpos(strtolower($string), 'ue'),
    '"HTML" word in uppercase'                     =>  str_replace("html", "HTML", $string),
    '"<INPUT>" word in uppercase'                  =>   str_replace("input", "INPUT", $string),
    'Strings converted to array'                   =>  preg_split("/[\s]+/", trim($string))
);

?>

<?php require('.includes/header.php'); ?>
      <div class="tab-content">
        <div class="tab-pane fade" id="spec">
          <h2>String Functions in PHP</h2>
          <p>The objective of this lab activity is to get acquainted with PHP's string manipulation functions.</p>
          <p>Download the full pdf instructions here : <br />
            <a href="https://www.dropbox.com/s/n30dkrrhheg8hh9/item413_lab6_string_functions_in_php.pdf" title="<?php echo LABTITLE . " - " . DESCRIPTION; ?>">
            <?php echo LABTITLE . " - " . DESCRIPTION; ?>
            </a>
            .</p>
        </div>
        <div class="tab-pane fade in active" id="app">
          <h3>String Functions in PHP<small>&rarr; Would it get any better than this?</small></h3>
          <?php if(isset($error)) : ?>
          <br />
            <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <h4>Error!</h4>
            <p><?php echo $error; ?><br/>
            If not, Please try again! &#9786;</p>
          </div>
<?php endif; ?>
<?php $t_string = trim($string); if(!empty($t_string)): ?>
          <table id="lab6" class="table table-striped table-bordered">
<tr>
  <th>No.</th>  <th>Description</th>  <th>Output</th>
</tr>
<?php $c = 0; foreach($output as $k => $v) : ?>
  <tr>
    <td><?php echo ++$c; ?></td>
    <td><?php echo htmlspecialchars($k); ?></td>
    <td><?php
    if(gettype($v) == 'array') {
      foreach($v as $index => $val) {
        echo htmlspecialchars("[{$index}] => {$val}");
        echo "<br />";
      }
    } elseif(empty($v)) {
      echo $booleanArray[(bool) $v];
    } else {
      echo htmlspecialchars($v);
    }
    ?></td>
  </tr>
<?php endforeach; ?>
          </table>
<?php endif; ?>
<br/>
          <form class="form-inline pull-right" action="lab6.php" method="get">
            <div class="input-prepend">
              <span class="add-on"><small>string</small></span>
              <input class="span3" id="prependedInput" type="text" placeholder="Enter any string e.g. 'Hello World!'" name="string">
            </div>
            <button type="submit" class="btn btn-orange" ><i class="icon-ok icon-white"></i> Apply &fnof;!</button>
            <button type="reset" class="btn btn-danger" ><i class="icon-refresh icon-white"></i> Reset</button>
          </form>
        </div>


<?php require('.includes/footer.php'); ?>