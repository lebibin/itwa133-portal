<?php session_start();

unset($_SESSION['customer']);
session_destroy();
header('Location: http://' . $_SERVER['SERVER_NAME'] . '/pe2/index.php');