<?php

define("LABTITLE", "Laboratory Activity No. 2");
define("DESCRIPTION", "Creating Basic PHP Script");
define("CURRENT", 'lab2.php');
define("IS_ERROR_PAGE", false);

define("NAME","Kevin Candelaria");
define("STUDENTNUMBER", "201020004");
define("ADDRESS", "Cainta, Rizal");
define("EMAILADDRESS", "octodoodle@bykev.in");
define("CONTACTNUMBER", "+639358899735");
define("WEBADDRESS", "http://bykev.in");

$school       = 'Far Eastern University - East Asia College';
$course       = 'BSIT spec. in Web Applications Development';
$yearlevel    = '3';
$dateofbirth  = 'August 19';
$gender       = 'Male';

?>

<?php require('.includes/header.php'); ?>
      <div class="tab-content">
        <div class="tab-pane fade" id="spec">
          <h2>Creating Basic PHP Script</h2>
          <p>The objective of this lab activity is to design a simple web page that will contain all your lab activities.</p>
          <p>Download the full pdf instructions here : <br />
            <a href="https://www.dropbox.com/s/h1cxifqd61c7brm/itwp103_lab2.pdf" title="<?php echo LABTITLE . " - " . DESCRIPTION; ?>">
            <?php echo LABTITLE . " - " . DESCRIPTION; ?>
            </a>
            .</p>
        </div>
        <div class="tab-pane fade in active" id="app">
  <img class="circle" alt="Octodoodle - Nick Automatic" title="Octodoodle - Nick Automatic!" src="/assets/img/octodoodle.png" width="190"/>
          <p class="main">
            <?php
              echo    "I am <i class='icon-user'></i> <span class='highlight'>" . NAME . "</span> from <i class='icon-map-marker'></i> <span class='highlight'>" . ADDRESS
                    . ".</span> Since I <i class='icon-heart'></i> building things on the web   I'm taking up the program / course / passion <i class=' icon-book'></i> <span class='highlight'>"
                    . $course . "</span>  at <i class=' icon-star'></i> <span class='highlight'>"
                    . $school . "</span>.  I'm currently in my <i class='icon-time'></i> <span class='highlight'>" . $yearlevel
                    . "<sup>rd</sup> year</span> from batch 2010, as  you can tell from my student number, <i class=' icon-barcode'></i> <span class='highlight'>"
                    . STUDENTNUMBER . "</span>.  I'm not like the typical <i class='icon-exclamation-sign'></i> <span class='highlight'>" . $gender
                    . "</span> everyone's trying to be.  I was born on <i class=' icon-gift'></i> <span class='highlight'>"
                    . $dateofbirth . "</span> so if you ever feel generous  feel free to buy me a gift."
                    . " You can contact me  via text <i class='icon-star'></i> <span class='highlight'>" . CONTACTNUMBER
                    . "</span> though I only have N3210,  via email <i class='icon-envelope'></i> <span class='highlight'><a href='mailto:" . EMAILADDRESS . "'>". EMAILADDRESS . "</a></span>"
                    . " though I don't reply,  and also via my site <i class=' icon-certificate'></i> <a href='" . WEBADDRESS . "' title='bykev.in'>bykev.in</a> though it doesn't really have a contact link."
                    . "    Just kidding, <i class=' icon-thumbs-up'></i> feel free to contact me! I don't bite.";
            ?>
          </p>

        </div>

        
<?php require('.includes/footer.php'); ?>