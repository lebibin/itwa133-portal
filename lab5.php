<?php

define("LABTITLE", "Laboratory Activity No. 5");
define("DESCRIPTION", "Iteration Structures");
define("CURRENT", 'lab5.php');
define("IS_ERROR_PAGE", false);
?>

<?php
  $dimension = (isset($_GET['dimension'])) ? filter_var($_GET['dimension'], FILTER_SANITIZE_NUMBER_INT) : 3;
  if($dimension < 1) {
      $final_dimension = 3;
      $error_head = "Less than MIN, sorry.";
      $error_body = "Why would anyone wished for a Magic Square with zero or negative dimensions i.e. a {$dimension} &times; {$dimension} Magic Square? <br/>It was degraded to 3 &times; 3 for everyone's convenience. Hope that's fine! &#9834; &#9835;";
  } elseif ($dimension > 51) {
      $final_dimension = 19;
      $error_head = "Greater than MAX, sorry.";
      $error_body = "You wished for a {$dimension} &times; {$dimension} Magic Square, yet I can only accomodate up to 51 &times; 51. <br/>The Magic Square was degraded to 19 &times; 19 for everyone's convenience. Hope that's fine! &#9834; &#9835;";
  } elseif ( $dimension % 2 == 0 ) {
      $final_dimension = $dimension + 1;
      $warning_head = "Even input detected.";
      $warning_body = "We only generate Magic Squares with ODD dimensions so I automatically added 1 to your input for you.";
  } elseif ( $dimension == 3 ) {
      $final_dimension = 3;
  } elseif ( $dimension == 1 ) {
      $final_dimension = $dimension;
      $warning_head = "Seriously?";
      $warning_body = "A 1 &times; 1 Magic Array? That doesn't seem to be magical at all!";
  }else {
      $final_dimension = $dimension;
  }


  function printMagicSquare($n) {
    if($n < 1) {
        $size = 3;
    } elseif ($n > 51) {
        $size = 19;
    } elseif ( $n % 2 == 0 ) {
        $size = $n + 1;
    } else {
        $size = $n;
    }

    $MATRIX = array();
    for( $row=0; $row<$size; $row++){
      for( $column=0; $column<$size; $column++ ){
        $MATRIX[$row][$column] = 0;
      }
    }


    $current_r_pos = 0;
    $current_c_pos = floor($size/2);
    $last_r_pos = 0;
    $last_c_pos = floor($size/2);

    for($n = 1; $n <= pow($size, 2); $n++ ){
      if( $n == 1 ) {
        $MATRIX[$current_r_pos][$current_c_pos] = $n;
      } else {

        $current_r_pos = $last_r_pos; $current_c_pos = $last_c_pos;
        $current_r_pos--; $current_c_pos--; // traverse up then left

        // if out of bounds then place it on the opposite column or row
        if($current_r_pos < 0 ) {
          $current_r_pos = $size-1;
        }
        if($current_c_pos < 0) {
          $current_c_pos = $size-1;
        }

        // if it is occupied then place it below the last placed value
        if($MATRIX[$current_r_pos][$current_c_pos]){
          $MATRIX[++$last_r_pos][$last_c_pos] = $n;
        } else {
          $MATRIX[$current_r_pos][$current_c_pos] = $n;
          $last_r_pos = $current_r_pos; $last_c_pos = $current_c_pos; // keep track of position of last value
        }
      }
    }

    $colsum = array();
    for( $row=0; $row<$size; $row++) {
      echo '<tr>';
      $rowsum = 0;
      $colsum[$row]=0;
      for( $column=0; $column<$size; $column++ ){
        echo "<td class='hghlght'><strong>" . number_format($MATRIX[$row][$column]) . "</strong></td>";
        $rowsum+=$MATRIX[$row][$column];
        $colsum[$row]+=$MATRIX[$column][$row];
      }
      echo "<td class='total'>" . number_format($rowsum) . "</td>";
      echo '</tr>';
    }
    echo '<tr>';
    foreach($colsum as $s){
      echo "<td class='total'>" . number_format($s) . "</td>";
    }
    echo '<td class="total">&Sigma;<sub><small>column</small></sub> / &Sigma;<sub><small>row</small></sub></td></tr>';
}
?>

<?php require('.includes/header.php'); ?>
      <div class="tab-content">
        <div class="tab-pane fade" id="spec">
          <h2>Magic Square</h2>
          <p>The objective of this lab activity is to create a Magic Square in PHP i.e. a grid of numbers that all add up to the same value.</p>
          <p>Download the full pdf instructions here : <br />
            <a href="https://www.dropbox.com/s/fmjcjjx4cj3emtc/lab5.pdf" title="<?php echo LABTITLE . " - " . DESCRIPTION; ?>">
            <?php echo LABTITLE . " - " . DESCRIPTION; ?>
            </a>
          .</p>
        </div>
        <div class="tab-pane fade in active" id="app">

          <h3><?php echo "{$final_dimension} &times; {$final_dimension}"; ?> Magic Square<small>&rarr; Is it really?</small></h3>
          <?php if(!empty($error_head)) : ?>
          <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <h4><?php echo $error_head; ?></h4>
            <?php echo $error_body; ?>
          </div>
          <?php endif; ?>
          <?php if(!empty($warning_head)) : ?>
          <div class="alert alert-block">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <h4><?php echo $warning_head; ?></h4>
            <?php echo $warning_body; ?>
          </div>
          <?php endif; ?>
          <table id="lab5" class="table table-striped table-bordered">
            <?php printMagicSquare($final_dimension); ?>
          </table>
          <hr />
          <form class="form-inline pull-right" action="lab5.php" method="get">
            <div class="input-prepend">
              <span class="add-on"><small>dimension</small></span>
              <input class="span3" id="prependedInput" type="text" placeholder="Enter number, e.g. 3 for 3x3 square" name="dimension">
            </div>
            <button type="submit" class="btn btn-orange" ><i class="icon-refresh icon-white"></i> Generate!</button>
          </form>
        </div>


<?php require('.includes/footer.php'); ?>