<?php

define("LABTITLE", "Laboratory Activity No. 3");
define("DESCRIPTION", "Working with Data Types and Operators");
define("CURRENT", 'lab3.php');
define("IS_ERROR_PAGE", false);

$myinteger 		= 	10;
$myfloat 		=	10.57;
$mystring 		=	'10 apples';
$myboolean 		=	TRUE;
$mynull 		=	NULL;
$myarray 		=	array(1,2,3);

$data = array(
	"myinteger"			=>	$myinteger,
	"myfloat"			=>	$myfloat,
	"mystring" 			=>	$mystring,
	"myboolean"			=>	$myboolean,
	"mynull" 			=>	$mynull,
	"myarray"			=>	$myarray
);

?>

<?php require('.includes/header.php'); ?>

      <div class="tab-content">
        <div class="tab-pane fade" id="spec">
          <h2>Working with Data Types and Operators</h2>
          <p>The objective of this lab activity is to work with PHP's Data Types and Operators.</p>
          <p>Download the full pdf instructions here : <br />
            <a href="https://www.dropbox.com/s/km86p9lvoysgh6c/itwp103_itwa133_lab3.pdf" title="<?php echo LABTITLE . " - " . DESCRIPTION; ?>">
            <?php echo LABTITLE . " - " . DESCRIPTION; ?>
            </a>
            .</p>
        </div>
        <div class="tab-pane fade in active" id="app">
            <table class="table table-striped table-bordered span10">
               <tr><th colspan=2>PHP Data Types</th></tr>
               <?php foreach($data as $key => $value) : ?>
               <tr><td>$<?php echo $key; ?></td> <td><?php echo var_dump($value); ?></td></tr>
               <?php endforeach; ?>
            </table>
<hr />
            <table class="table table-striped table-bordered span10">
               <tr><th colspan=2>PHP Arithmetic Operators</th></tr>

               <tr><td>$myinteger + 10</td><td><?php echo var_dump($myinteger + 10); ?></td></tr>
               <tr><td>$myinteger - 5</td>   <td><?php echo var_dump($myinteger - 5); ?></td></tr>
               <tr><td>$myinteger * 2</td>   <td><?php echo var_dump($myinteger * 2); ?></td></tr>
               <tr><td>$myinteger / 3</td>   <td><?php echo var_dump($myinteger / 3); ?></td></tr>
               <tr><td>$myboolean + 1</td>   <td><?php echo var_dump($myboolean + 1); ?></td></tr>
               <tr><td>$mystring * 2</td> <td><?php echo var_dump($mystring * 2); ?></td></tr>

            </table>
<hr />
            <table class="table table-striped table-bordered span10">
               <tr><th colspan=2>PHP Bitwise Operators</th></tr>

               <tr><td>$myinteger &amp; 8</td>      <td><?php echo var_dump($myinteger & 8); ?></td></tr>
               <tr><td>$myinteger | 8</td>      <td><?php echo var_dump($myinteger | 8); ?></td></tr>
               <tr><td>$myinteger &lt;&lt; 1</td>  <td><?php echo var_dump($myinteger << 1); ?></td></tr>
               <tr><td>$myinteger >> 1</td>  <td><?php echo var_dump($myinteger >> 1); ?></td></tr>

            </table>
<hr />
            <table class="table table-striped table-bordered span10">
               <tr><th colspan=2>PHP Comparison Operators</th></tr>

               <tr><td>$myinteger == 10</td>       <td><?php echo var_dump($myinteger == 10); ?></td></tr>
               <tr><td>$myinteger > 10</td>        <td><?php echo var_dump($myinteger > 10); ?></td></tr>
               <tr><td>$myinteger >= 10</td>       <td><?php echo var_dump($myinteger >= 10); ?></td></tr>
               <tr><td>$myinteger &lt; 20</td>        <td><?php echo var_dump($myinteger < 20); ?></td></tr>
               <tr><td>$myinteger &lt;= 20</td>       <td><?php echo var_dump($myinteger <= 20); ?></td></tr>
               <tr><td>$myinteger == $mystring</td>   <td><?php echo var_dump($myinteger == $mystring); ?></td></tr>
            <tr><td>$myinteger === $mystring</td>  <td><?php echo var_dump($myinteger === $mystring); ?></td></tr>

            </table>
<hr />
            <table class="table table-striped table-bordered span10">
               <tr><th colspan=2>PHP Logical Operators</th></tr>

               <tr><td>$myinteger >= 10 and $myinteger &lt;= 20</td>    <td><?php echo var_dump($myinteger >= 10 and $myinteger <= 20); ?></td></tr>
               <tr><td>$myinteger == 10 or $myinteger == 20</td>     <td><?php echo var_dump($myinteger == 10 or $myinteger == 20); ?></td></tr>
               <tr><td>$myinteger == 10 xor $myinteger == 20</td>    <td><?php echo var_dump($myinteger == 10 xor $myinteger == 20); ?></td></tr>

            </table>
        </div>



<?php require('.includes/footer.php'); ?>