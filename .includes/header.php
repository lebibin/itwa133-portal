<?php

if(stripos($_SERVER['REQUEST_URI'], '.includes')){
  $m = strtolower("Roses Are Red That Much Is True, But Violets Are Purple Not Freaking Blue!");
  die($m);
}

$links = array(
  'lab1.php'                     => 'Tabular Student Grade',
  'lab2.php'                     => 'Creating Basic Script',
  'lab3.php'                     => 'Working with Data Types and Operators',
  'lab4.php'                     => 'Functions and Control Structures',
  'lab5.php'                     => 'Iteration Structures',
  'lab6.php'                     => 'String Functions in PHP',
  'lab7.php'                     => 'Regular Expressions in PHP',
  'pe/input.php'                 => 'Square and Cube of a Number',
  'pe2/index.php'                => 'WEBPROG Travel Agency',
  // 'lab8.php'                     => 'Jaunty Jackalope',
  // 'lab9.php'                     => 'Karmic Koala',
  // 'lab10.php'                    => 'Lucid Lynx',
  // 'lab11.php'                    => 'Maverick Meerkat',
  // 'lab12.php'                    => 'Natty Narwhal',
  // 'lab13.php'                    => 'Oneiric Ocelot',
  // 'lab14.php'                    => 'Precise Pangolin',
  // 'lab15.php'                    => 'Quantal Quetzal',
  // 'lab16.php'                    => 'Raring Ringtail',
);

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo LABTITLE; ?> - Portal by Kevin Candelaria</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Portal for Web Applications Course - FEU East Asia College. <?php echo DESCRIPTION; ?>">
    <meta name="author" content="Kevin Candelaria">
    <meta name="keywords" content="portal.bykev.in, kevin candelaria, feu-eac, far eastern university east asia college, itwa133">
    <link rel="icon" href="../assets/img/favicon.ico" type="image/x-icon">

    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Finger+Paint' rel='stylesheet' type='text/css'>
    <link href="http://<?php echo $_SERVER['SERVER_NAME']; ?>/assets/css/bootstrap.css" rel="stylesheet">
    <link href="http://<?php echo $_SERVER['SERVER_NAME']; ?>/assets/css/responsive.css" rel="stylesheet">
    <link href="http://<?php echo $_SERVER['SERVER_NAME']; ?>/assets/css/main.css" rel="stylesheet">
  </head>

  <body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">

      <a class="brand doodle" href="/" title="Portal By Kevin Candelaria">$portal->by('kevin')</a>
      <ul class="nav">

        <?php $c=1;foreach($links as $href => $text): ?>
        <li <?php if( $href == CURRENT ) echo 'class="active"'; ?>>
        <?php if($href == 'pe/input.php') : ?>
        <a href="http://<?php echo $_SERVER['SERVER_NAME']; ?>/<?php echo $href; ?>" title="<?php echo "Midterm - Practical Exam - {$text}"; ?>">PE</a></li>
        <?php elseif($href == 'pe2/index.php') : ?>
        <a href="http://<?php echo $_SERVER['SERVER_NAME']; ?>/<?php echo $href; ?>" title="<?php echo "Midterm - Practical Exam - {$text}"; ?>">PE2</a></li>
        <?php else : ?>
        <a href="http://<?php echo $_SERVER['SERVER_NAME']; ?>/<?php echo $href; ?>" title="<?php echo "Laboratory Activity No. {$c} - {$text}"; ?>"><?php echo $c; ?></a></li>
        <?php endif; ?>
        <?php $c++; endforeach; ?>

      </ul>
      <ul class="nav pull-right">
        <li><a title="About Kevin Candelaria" href="#aboutMe" role="button" data-toggle="modal"><i class="icon-user"></i></a></li>
        <li><a title="Contact Kevin Candelaria" href="#contactMe" role="button" data-toggle="modal"><i class="icon-envelope"></i></a></li>
        <li><a title="Chip in" href="#chipIn" role="button" data-toggle="modal"><i class="icon-heart"></i></a></li>
      </ul>
      </div>
 
    </div>
  </div>


    <div class="content container">

<?php if(CURRENT != '/' && ! IS_ERROR_PAGE ) : ?>
     <div id="title" class="hero-unit">
      <h1>&#8220; <?php echo DESCRIPTION; ?> &#8221;</h1>
      <p><small><?php echo LABTITLE ?></small></p>
    </div>


<div class="row">
  <div id="content" class="span12">
    <div class="tabbable">
      <ul class="nav nav-tabs">
        <li><a href="#spec" data-toggle="tab">Instruction</a></li>
        <li class="active"><a href="#app" data-toggle="tab">Application</a></li>
        <li><a href="#comment" data-toggle="tab">Comment</a></li>
      </ul>

<?php endif; ?>