
      <?php

    define("LABTITLE", "Midterm - Practical Exam");
    define("DESCRIPTION", "Square and Cube of a Number");
    define("CURRENT", 'pe/input.php');
    define("IS_ERROR_PAGE", false);
    
    ?>
<?php require('../.includes/header.php'); ?>

<div class="tab-content">
    <div class="tab-pane fade" id="spec">
          <h2>Square and Cube of a Number</h2>
          <p>Write a PHP script to produce a neatly printed table containing the squares and cubes of numbers from
1 to 25 only.</p>
          <p>Download the full pdf instructions here : <br />
            <a href="https://www.dropbox.com/s/lut0mgwzh8fp8es/itwp103_itwa133_midterm_practical_exam.pdf" title="<?php echo LABTITLE . " - " . DESCRIPTION; ?>">
            <?php echo LABTITLE . " - " . DESCRIPTION; ?>
            </a>
            .</p>
    </div>
    <div class="tab-pane fade in active" id="app">
    <div id="pe">
        <h3>Exponents.<small>&rarr; Same old. Same old.</small></h3>
        <hr/>
        <form action="output.php" method="post" class="form-horizontal">
        <div class="control-group">
            <label class="control-label" for="number">Number ( n )</label>
                <div class="controls">
            <input type="text" name="number" placeholder="Enter a positive integer. . .">
                </div>
            </div>
        <div class="control-group">
            <label class="control-label" class="checkbox inline">Power ( n<sup>p</sup> )</label>
            <div class="controls">
                <label class="checkbox inline">
                  <input type="checkbox" name="pow[]" value="square"> n<sup>2</sup>
                </label>
                <label class="checkbox inline">
                  <input type="checkbox" name="pow[]" value="cube"> n<sup>3</sup>
                </label>
                <label class="checkbox inline">
                  <input type="checkbox" name="pow[]" value="fourth"> n<sup>4</sup>
                </label>
                <label class="checkbox inline">
                  <input type="checkbox" name="pow[]" value="fifth"> n<sup>5</sup>
                </label>
                <label class="checkbox inline">
                  <input type="checkbox" name="pow[]" value="tenth"> n<sup>10</sup>
                </label>
                </div>
            </div>
        <hr />
        <div class="control-group pull-right">
            <div class="controls">
                <button type="submit" class="btn btn-orange"><i class="icon-white icon-ok"></i> Submit</button>
            </div>
        </div>
        </form>
    </div>
    </div>



<?php require('../.includes/footer.php'); ?>