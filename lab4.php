<?php

define("LABTITLE", "Laboratory Activity No. 4");
define("DESCRIPTION", "Functions and Control Structures");
define("CURRENT", 'lab4.php');
define("IS_ERROR_PAGE", false);

?>

<?php require('.includes/header.php'); ?>

<?php
  // Base deck that'll contain the base deck of 52 cards
  $base_deck = array();

  // Array of cards suits
  $suits = array('spades', 'clubs', 'hearts', 'diamonds');


  // Initialize a Number of Players variable
  // or get it from the GET variable players
  $num_playahs = 9;
  if(isset($_GET['players']) && preg_match('/^[\d]+$/', $_GET['players'])){
    $p = filter_var($_GET['players'], FILTER_SANITIZE_NUMBER_INT);
    $p = strip_tags($p);
    $num_playahs = ($p >= 1 && $p <= 26) ? $p : 9;
  }


  // Seed the base deck to contain the cards
  // format ex. : $base_deck['hearts'][9]
  for($i=1; $i<=13; $i++){
    foreach($suits as $suit){
      $base_deck[$suit][$i] = $i;
    }
  }

  // Setup a static deck that'll contain the deck in play
  static $deck_in_use;
  $deck_in_use = $base_deck;

  // Deals a random card
  // returns an associative array with 'suit', 'value',
  // 'image and 'score' keys
  function deal_card(){
    global $suits; global $deck_in_use;

    do {
      // randomly select from base_deck
        $suit = $suits[rand(0,3)];
        $value = rand(1,13);
        // check if the card isn't dealt yet
        if ((boolean) $deck_in_use[$suit][$value]) {
          $score = $value;
          $deck_in_use[$suit][$value] = 0;
          break;
        }
    } while(1);

    // Setup the card array to be returned
    $card = array(
      'suit'    => $suit,
      'value'   => $value,
      'score'   => $score,
    );

    // Maps the score to the equivalent image number representation
    if($card['suit'] == 'clubs') {
      $card['image'] = $value;
    } elseif($card['suit'] == 'spades') {
      $card['image'] = 13 + $value;
    } elseif($card['suit'] == 'hearts') {
      $card['image'] = 26 + $value;
    } elseif($card['suit'] == 'diamonds') {
      $card['image'] = 39 + $value;
    }

    // Switch face card value to string equivalent
    switch ($card['score']){
        case 1 :
          $card['value'] = "Ace";
          break;
        case 11 :
          $card['value'] = "Jack";
          break;
        case 12 :
          $card['value'] = "Queen";
          break;
        case 13 :
          $card['value'] = "King";
          break;
        default :
          break;
      }

    // Check if the card is 10 or is a face card
    // Sets its score to 0 if so
    if($card['score'] >= 10) {
      $card['score'] = 0;
    }

    // return the associative array
    // of the random and unique card dealt
    return $card;
  }

  // function to get html entity of the suit
  function getHTMLEntity($suit){
    if($suit == 'clubs') {
      $entity = '&clubs;';
    } elseif($suit == 'spades') {
      $entity = '&spades;';
    } elseif($suit == 'hearts') {
      $entity = '&hearts;';
    } elseif($suit == 'diamonds') {
      $entity = '&diams;';
    }
    return $entity;
  }


// Initialize player array that will contain each player's score
// Create an array index 'total' that will contain a player's total score
$player = array();
for($i=1; $i<=$num_playahs; $i++){
  $player[$i]['total'] = 0;
  for($h=0;$h<2;$h++){ // Loop twice since you can only be dealt two cards
    $player[$i][$h] = deal_card();
    $player[$i]['total'] += $player[$i][$h]['score'];
  }
  if($player[$i]['total'] > 9)
    $player[$i]['total'] -= 10;
}

// Get the highest score from all players
// store it into $highest
$highest = 0;
for($c=1; $c<=$num_playahs; $c++){
  if($player[$c]['total'] > $highest) {
    $highest = $player[$c]['total'];
    }
}

// Initialize a different player name for each player
// Should be equal or greater than $num_playahs
$playah = array(
      'Linus Torvalds',
      'Taylor Otwell',
      'Yukihiro Matsumoto',
      'Drew Houston',
      'Arash Ferdowsi',
      'Steve Jobs',
      'John Resig',
      'Ryan Dahl',
      'Jeremy Ashkenas',
      'Mark Shuttleworth',
      'Tom Preston-Werner',
      'Jesper Noehr',
      'Brendan Eich',
      'Larry Page',
      'Sergey Brin',
      'Blake Ross',
      'Rasmus Lerdorf',
      'Mark Otto',
      'Jacob Thornton',
      'Tim Berners-Lee',
      'Michael Lovitt',
      'Aaron Swartz',
      'Gabe Newell',
      'Mike Harrington',
      'Gina Trapani',
      'Jimmy Wales',
      'Jeff Atwood',
      'Icefrog'
      );

// Shuffle playah array to give a randomized
// player order on each reload
shuffle($playah);

?>
      <div class="tab-content">
        <div class="tab-pane fade" id="spec">
          <h2>Lucky 9 Card Game</h2>
          <p>The objective of this lab activity is to apply functions and control structures in creating a simple card game of lucky Nine (9). Four players are given two random cards each. Player having the highest total card value wins the game. There can be multiple winners for each turn provided the total card value is not zero (0). The face value of Jacks, Queens and Kings are zero (0) point and Aces are one (1) point. Example King of Hearts (0) + Two of Diamonds (2) will yield two points (2).</p>
          <p>Download the full pdf instructions here : <br />
            <a href="https://www.dropbox.com/s/8qb9em4w4enep6p/itwp103_itwa132_lab_activity_4_lucky_9_card_game.pdf" title="<?php echo LABTITLE . " - " . DESCRIPTION; ?>">
            <?php echo LABTITLE . " - " . DESCRIPTION; ?></a> plus the card images <a href="https://www.dropbox.com/s/b9940b1a08lwxo9/cards.zip" title="Card Images">here</a>.</p>
        </div>
        <div class="tab-pane fade in active" id="app">
          <h3>Lucky Nine <small>&rarr; I bet these players don't need luck.</small></h3>
          <table id="cards" class="table table-bordered span12">
            <tr>
              <th><i class="icon-user"></i><br/>Player</th>
              <?php for($i=1;$i<=$num_playahs;$i++) : ?>
              <?php if($player[$i]['total'] == $highest && (bool) $player[$i]['total'])
                  echo '<th class="win">';
                else
                  echo '<th class="lose">';
              ?>
              <?php echo ' <sup>' . $i . '</sup> ' . $playah[$i]; ?></th>
              <?php endfor; ?>
            </tr>

            <tr>
              <th rowspan="2">Hand</th>
              <?php for($i=1;$i<=$num_playahs;$i++) : ?>
              <?php if($player[$i]['total'] == $highest && (bool) $player[$i]['total'])
                  echo '<td class="win">';
                else
                  echo '<td class="lose">';
              ?>
                <img alt="<?php echo $player[$i][0]['value'] . ' of ' . ucfirst($player[$i][0]['suit']) . ' ' . getHTMLEntity($player[$i][0]['suit']); ?>" title="<?php echo $player[$i][0]['value'] . ' of ' . ucfirst($player[$i][0]['suit']) . ' ' . getHTMLEntity($player[$i][0]['suit']); ?>" src="./assets/img/<?php echo $player[$i][0]['image']; ?>.png"><br />
                <small><?php echo $player[$i][0]['value'] . ' of ' . ucfirst($player[$i][0]['suit']) . ' ' . getHTMLEntity($player[$i][0]['suit']); ?></small>
              </td>
              <?php endfor; ?>
            </tr>

            <tr>
              <?php for($i=1;$i<=$num_playahs;$i++) : ?>
              <?php if($player[$i]['total'] == $highest && (bool) $player[$i]['total'])
                  echo '<td class="win">';
                else
                  echo '<td class="lose">';
              ?>
                <img alt="<?php echo $player[$i][1]['value'] . ' of ' . ucfirst($player[$i][1]['suit'])  . ' ' . getHTMLEntity($player[$i][1]['suit']); ?>" title="<?php echo $player[$i][1]['value'] . ' of ' . ucfirst($player[$i][1]['suit'])   . ' ' . getHTMLEntity($player[$i][1]['suit']);; ?>" src="./assets/img/<?php echo $player[$i][1]['image']; ?>.png"><br />
                <small><?php echo $player[$i][1]['value'] . ' of ' . ucfirst($player[$i][1]['suit'])  . ' ' . getHTMLEntity($player[$i][1]['suit']);; ?></small>
              </td>
              <?php endfor; ?>
            </tr>

            <tr>
              <th>Score</th>
              <?php for($i=1;$i<=$num_playahs;$i++) : ?>
              <?php if($player[$i]['total'] == $highest && (bool) $player[$i]['total'])
                  echo '<td class="win">';
                else
                  echo '<td class="lose">';
              ?>
              <?php echo $player[$i]['total']; ?></td>
              <?php endfor; ?>
            </tr>

            <tr>
              <th><i class="icon-certificate"></i><br/>Standing</th>
              <?php for($i=1;$i<=$num_playahs;$i++) : ?>
              <?php if($player[$i]['total'] == $highest && (bool) $player[$i]['total'])
                  echo '<td class="win"> <i class="icon-thumbs-up"></i> Won';
                else
                  echo '<td class="lose"> <i class="icon-thumbs-down"></i> Lost';
              ?>
            </td>
              <?php endfor; ?>
            </tr>
            <tr>
          <td id="form-row" colspan="<?php echo $num_playahs + 1;?>">
            <p class="pull-left">Note: There are only 52 cards in a deck so there can only be 26 players max.</p>
            <form class="form-inline pull-right" action="/lab4.php">
              <input type="text" name="players" placeholder="Number of Players : <?php echo $num_playahs; ?>"/>
            <button type="submit" class="btn btn-orange"><i class="icon-refresh icon-white"></i> Play again!</button>
            </form>
          </td>
            </tr>
          <?php
            if( $num_playahs == 1):
          ?>
          <tr>
            <th colspan="<?php echo $num_playahs + 1;?>"><p class="lead">Forever alone. : (</p></th>
          </tr>
        <?php endif; ?>
          </table>
        </div>


<?php require('.includes/footer.php'); ?>