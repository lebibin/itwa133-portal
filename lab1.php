
     	<?php

     	define("LABTITLE", "Laboratory Activity No. 1");
		define("DESCRIPTION", "Create a simple Table w/ PHP");
		define("CURRENT", 'lab1.php');
    	define("IS_ERROR_PAGE", false);
		
		$name		=	'Kevin Candelaria';
		$course		=	'BS Information Technology w/ Specialization in Web Application Development';
		$birthdate	= 	new DateTime('1991-08-19');
		$today		=	new DateTime(date('Y-m-d'));
		$age 		= 	$birthdate->diff($today)->format('%Y');
		$quiz1		=	45;
		$quiz2		=	82;
		$quiz3		=	17;
		$quiz_ave	=	(($quiz1/50) + ($quiz2/100) + ($quiz3/20)) / 3 *100;
		?>
<?php require('.includes/header.php'); ?>

<div class="tab-content">
    <div class="tab-pane fade" id="spec">
          <h2>Getting Started with XAMPP</h2>
          <p>The objective of this lab activity is to get acquainted with XAMPP.</p>
          <p>Download the full pdf instructions here : <br />
            <a href="https://www.dropbox.com/s/dhahys5xvex2oz8/itwp103_lab_activity_getting_started_with_xmapp.doc" title="<?php echo LABTITLE . " - " . DESCRIPTION; ?>">
            <?php echo LABTITLE . " - " . DESCRIPTION; ?>
            </a>
            .</p>
    </div>
    <div class="tab-pane fade in active" id="app">
		<table id="lab1">
			<tr>
				<td>Name: </td>
				<td><?php echo $name; ?></td>
			</tr>
			
			<tr>
				<td>Course: </td>
				<td><?php echo $course; ?></td>
			</tr>

			<tr>
				<td>Birthdate: </td>
				<td><?php echo $birthdate->format('F d, Y'); ?></td>
			</tr>

			<tr>
				<td>Age: </td>
				<td><?php echo $age; ?></td>
			</tr>

			<tr>
				<td>Quiz 1: </td>
				<td><?php echo $quiz1; ?> / 50</td>
			</tr>

			<tr>
				<td>Quiz 2: </td>
				<td><?php echo $quiz2; ?> / 100</td>
			</tr>	

			<tr>
				<td>Quiz 2: </td>
				<td><?php echo $quiz3; ?> / 20</td>
			</tr>
			
			<tr>
				<td>Quiz Ave: </td>
				<td><?php printf("%.2f", $quiz_ave); ?></td>
			</tr>

			</table>
    </div>



<?php require('.includes/footer.php'); ?>