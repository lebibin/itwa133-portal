<?php session_start(); 

    define("LABTITLE", "Mini - Flight Reservation");
    define("DESCRIPTION", "WEBPROG Travel Agency");
    define("CURRENT", 'pe2/index.php');
    define("IS_ERROR_PAGE", true);

	if( ! isset($_SESSION['customer']) ) {
	    header('Location: http://' . $_SERVER['SERVER_NAME'] . '/pe2/index.php');
	}

	$sum = 0;

	if($_SESSION['customer']['Flight Direction'] == 'One-Way') $base = 850;
	else $base = 1700;

?>

<?php require('../.includes/header.php'); ?>

          <div id="title" class="hero-unit">
            <h1>WEBPROG Travel Agency<small> &rarr; Fare reservation information</small></h1>
            <hr />
          <table class='pe2-result table table-bordered'>
			<tr>
				<th>Field</th>
				<th>Data</th>
			</tr>
			<?php foreach($_SESSION['customer'] as $key => $value): ?>
			<tr>
				<?php if(is_array($value)) : ?>
				<td><?php echo $key; ?></td>
				<td>
					<?php echo implode($value, ', '); ?>
				</td>
				<?php elseif($key == 'Flight Direction') : ?>
				<td><?php echo $key; ?></td>
				<td><?php echo $value; ?>
					<?php
						if($value == 'One-Way') {
							echo ' ( &#43; &#8369;850.00 )';
							$sum += 850;
						} else {
							echo ' ( &#43; &#8369;1700.00 )';
							$sum += 1700;
						}
					?>
				</td>

				<?php elseif($key == 'Companion 1') :
					if($value == 'No') continue; ?>
				<td><?php echo $key; ?></td>
				<td><?php echo $value; ?>
					<?php
						if($value != 'No') {
							if($value == 'Child') {
								$charge = 0.05 * $base;
							} elseif($value == 'Senior Citizen') {
								$charge = 0.08 * $base;
							} else {
								$charge = 1 * $base;	
							}
							$sum += $charge;
							echo ' ( &#43; &#8369;' . number_format($charge, 2) . ' )';
						} 
					?>
				</td>

				<?php elseif($key == 'Companion 2') :
					if($value == 'No') continue; 
					?>
				<td><?php echo $key; ?></td>
				<td><?php echo $value; ?>
					<?php
						if($value != 'No') {
							if($value == 'Child') {
								$charge = 0.05 * $base;
							} elseif($value == 'Senior Citizen') {
								$charge = 0.08 * $base;
							} else {
								$charge = 1 * $base;	
							}
							$sum += $charge;
							echo ' ( &#43; &#8369;' . number_format($charge, 2) . ' )';
						}
					?>
				</td>

				<?php elseif($key == 'Origin') : ?>
				<td><?php echo $key; ?></td>
				<td><?php echo $value; ?>
					<?php
						if($value == 'Manila') {
							echo ' ( &#45; &#8369;50.00 )';
							$sum -= 50;
						}
					?>
				</td>

				<?php elseif($key == 'Total Weight') : ?>
				<td><?php echo $key; ?></td>
				<td><?php echo $value . ' kg'; ?>
					<?php
						if($value >= 6 && $value <= 10) {
							echo ' ( &#43; &#8369;300.00 )';
							$sum += 300;
						} elseif($value >= 11 && $value <= 15) {
							echo ' ( &#43; &#8369;600.00 )';
							$sum += 600;
						} elseif($value >= 16 && $value <= 20) {
							echo ' ( &#43; &#8369;900.00 )';
							$sum += 900;
						} else ;
					?>
				</td>
				<?php else : ?>
				<td><?php echo $key; ?></td>
				<td><?php echo $value; ?></td>
				<?php endif; ?>
			</tr>
			<?php endforeach; ?>
			<tr>
				<th>Total Payable</th>
				<th><?php echo '&#8369;' . number_format($sum, 2); ?></th>
			</tr>
          </table>
            <hr />
 <a href="./back.php" class="btn btn-orange pull-right"><i class="icon-white icon-arrow-left"></i> Back</a>

          </div>

<?php require('../.includes/footer.php'); ?>