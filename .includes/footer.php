<?php
if(stripos($_SERVER['REQUEST_URI'], '.includes')){
  $m = strtolower("Roses Are Red That Much Is True, But Violets Are Purple Not Freaking Blue!");
  die($m);
}
?>
<?php if(CURRENT != '/' && ! IS_ERROR_PAGE ) : ?>
    <div class="tab-pane fade" id="comment">
      <div id="disqus_thread"></div>
      <script type="text/javascript">
          var disqus_shortname = 'portalbykevin';
          (function() {
              var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
              dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
              (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
          })();
      </script>
      <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
      <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
    </div>
  </div>
</div>
</div>
</div>
 <?php endif; ?>


<div id="aboutMe" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="aboutLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="aboutLabel">&#8220; About &#8221;</h3>
  </div>
  <div class="modal-body">
    <p>I'm <strong>Kevin Candelaria</strong>.</p>
    <p>I'm currently an undergrad at Far Eastern University -- East Asia College.</p>
    <p>This site is solely for compiling the activities we've done in our Web Programming class.</p>
    <p>If you're interested in doing the activities yourself, I've listed not only the application I made but also a link to download the corresponding document our professor had provided us with for each activity. I dare you to DIY, though I don't think it'll pose any challenge. : )</p>
    <p>I guess that's it? Feel free to mock what I made. Haters gonna hate, lol jk.</p>
    <p><del>Peace out.</del></p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-orange" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>


  <div id="contactMe" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h3 id="contactLabel">&#8220; Contact &#8221;</h3>
    </div>
    <div class="modal-body">
      <p>I'm online almost everyday, so if there's anything(rant, comment, suggestion, violent reaction, blabber, whatever) feel free to send me a message. 
        Though my reply, is not guaranteed.</p>
      <p>There are several ways you can contact me:</p>
      <ul>
        <li><a href="mailto:octodoodle@bykev.in" title="Send me an e-mail">Email</a></li>
        <li><a href="http://facebook.com/b1b1n" title="Since everyone seems to be on Facebook">Facebook</a></li>
      </ul>
    </div>
    <div class="modal-footer">
      <button class="btn btn-orange" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
  </div>


  <div id="chipIn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="chipInLabel" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h3 id="chipInLabel">&#8220; Chip in! &#8221;</h3>
    </div>
    <div class="modal-body">
      <p>If you're feeling generous, or would just like to help me out, feel free to check out these
        awesome services :</p>
      <ul>
        <li><a href="http://asmallorange.com" title="Web Host w/ an ever-reliable Customer Support">asmallorange</a> - Been hosting this site(among others) 
          with them, and fortunately I haven't encountered any issues.
        So if you'd like to host your personal sites or small projects somewhere, I wouldn't recommend any other host.</li>
        <li><a href="http://db.tt/Cfrkry2" title="Need I say more?">Dropbox</a> - If you don't know Dropbox, then you're definitely missing out. 
          Remember that time you accidentally left an important file at home, in your laptop? With Dropbox,
           that's nonsense since you'll always have access to your much needed files.</li>
      </ul>
    </div>
    <div class="modal-footer">
      <button class="btn btn-orange" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
  </div>
</div>

        <div id="footer" >
            <p>&copy; <?php echo date('Y'); ?> <a href="http://bykev.in" title="Doodle with Code">code-doodlers </a></p>
       </div>

  <script type="text/javascript" src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/assets/js/jquery.js"></script>
  <script type="text/javascript" src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/assets/js/app.min.js"></script>
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33571766-4']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
  </body>
</html>
